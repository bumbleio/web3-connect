// basic web3 setup checks for latest web3 client, legacy or no client

window.addEventListener('load', function () {
desiredNetwork = '1';
    if (window.ethereum) {
        console.log("got here 1")
        window.web3 = new Web3(ethereum);
        ethereum.enable()
        .catch(function (reason) {
              // The user didn't want to sign in!
              alert('Please accept connection to dapp')
              console.log(reason)
          })
        .then(function (accounts) {
            if (ethereum.networkVersion !== desiredNetwork) {
                alert('This application requires the main network, please switch it in your MetaMask UI and reload page.')
              } else {   
                console.log(accounts);
                dashboard() 
              };  
        });
            // var myContract = new web3.eth.Contract(abi,contractAddress);
            //myContract.methods.RegisterInstructor('11','Ali')
    }
    // Legacy dapp browsers...
    else if (window.web3) {
        window.web3 = new Web3(web3.currentProvider);
        this.console.log("legacy dapp browser")
        dashboard() 
        // Acccounts always exposed
        //web3.eth.sendTransaction({/* ... */});
    }
    // Non-dapp browsers...
    else {
        console.log('Non-Ethereum browser detected. You should consider trying MetaMask!');
    }

});


function dashboard() {

    version = web3.version.api;   
    networkON = ethereum.networkVersion;
    document.getElementById("network").innerHTML = "Ethereum network connected to: " + networkON;
    document.getElementById("web3v").innerHTML = "web 3 version: " + version;
    web3.eth.getBlockNumber(function(error, result){ 
        console.log(result);
        document.getElementById("blocknumber").innerHTML = "current block number:" + result ;
        });
};

// checks for metamask network change and reloads page - this needs work and seems to reload more than once
ethereum.on('networkChanged', function (network) {
    //reload page on network change
    location.reload(true);
    console.log("networkchanged");
  });








